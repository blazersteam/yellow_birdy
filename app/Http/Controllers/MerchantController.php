<?php

namespace App\Http\Controllers;


use App\Contact;
use App\Event;
use App\Http\Requests;
use App\Section;
use App\WebmasterSection;
use App\User;
use Auth;
use Illuminate\Http\Request;

class MerchantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // General for all pages
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        
        // General END

        if (@Auth::user()->permissionsGroup->id==3) { // Ids definition  1 admin , 2 webmaster , 3 users
            $merchantinfo = User::where('permissions_id', '=', Auth::user()->permissionsGroup->id)
                ->find(Auth::user()->id);
        } else {
            //List of all Webmails
            return redirect()->action('HomeController@index');
        }
        return view('backEnd.merchant',
            compact("GeneralWebmasterSections","merchantinfo"));
    }

    
}
